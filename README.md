# PostgreSQL Waffles
This extension was created for education purpose only. It will look for several
recipes, let you choose the one you'd like to see and display the ingredients
and step to make the recipe.

## Installation
### Pre-requisites
As parsing a website is not easy, we'll use the html-xml utilities from w3c
see [https://www.w3.org/Tools/HTML-XML-utils/](https://www.w3.org/Tools/HTML-XML-utils/).

The extension has been tested against Postgres 14.

You need to have Postgres already installed.
You need to get the extension and install it:

    git clone <pgwaffles_project>
    cd pgwaffles
    make install

### Creating the extension
Then you can create the extension

    create extension pgwaffles;

## Uninstalling
Simply use the `drop extension` statement.

